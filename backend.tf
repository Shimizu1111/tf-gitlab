terraform {
  backend "s3" {
    bucket  = "tf-gitlab"
    key     = "terraform.tfstate"
    region  = "ap-northeast-1"
    profile = "my-aws-profile"
  }
}