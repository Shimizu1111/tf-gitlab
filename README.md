# terrafor_gitlab

### 今後の学習手順
* [GitLabに関する知見色々](https://dev.classmethod.jp/tags/gitlab/)
* [GitLabについての理解(クラスメソッドの記事)](https://dev.classmethod.jp/tags/gitlab/)
* [GitLabCIについての理解]()
* [terraform入門](https://dev.classmethod.jp/articles/terraform-getting-started-with-aws/)
* [Terraform と GitLab CIの入門](https://techblog.istyle.co.jp/archives/6265)


### TIPS
* terraformのバージョン1系統以前(0.15.5など)のバージョンを取得する方法
[参考記事](https://dev.classmethod.jp/articles/tfenv-on-m1-mac/)
[terraformのリポジトリ](https://releases.hashicorp.com/terraform/)
```
which tfenv
ls /opt/homebrew/Cellar/tfenv/2.2.3/versions
mkdir /opt/homebrew/Cellar/tfenv/2.2.3/versions/{バージョン名}
cd /opt/homebrew/Cellar/tfenv/2.2.3/versions/{バージョン名}
wget {terraformのリポジトリから指定のバージョンのURLを選択}
unzip {wgetで取得したファイル名}
tfenv list
```
→tfenv listで取得したいバージョンが入っていることを確認

